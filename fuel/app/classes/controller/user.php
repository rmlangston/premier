<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_User extends Controller_Template
{
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$users = Model_User::find('all');
		//If form submitted
		if(Input::post('send')) {
			// Set validation and error messages
			$val = Validation::forge(); 
   			$val->add('name', 'Name')->add_rule('required')->add_rule('valid_string', array('alpha','numeric','spaces')); 
			$val->set_message('valid_string', 'The field :label must not only contain Uppercase, Lowercase, and Numeric characters.');
			$val->set_message('required', 'The field :label is required');
			// If validation met
			if ($val->run()) { 
			
				//Create new user 
				$user = new Model_User();
				//User name from form
				$user->name = Input::post('name');
				//User image from form
				// Custom configuration for this upload
				$config = array(
				    'path' => DOCROOT.'files',
				    'randomize' => true,
				    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
				);

				// process the uploaded files in $_FILES
				Upload::process($config);

				// if there are any valid files
				if (Upload::is_valid())
				{
				    // save them according to the config
				    Upload::save();
			    
				    // call a model method to update the database
				    $user->image = Upload::get_files(0)['saved_as'];  
 
				} 
			
				$files = Upload::get_files();
				foreach( $files  as $key=>$file ) {
	
					// getting the full path of the file

					$filepath=$file['saved_to'].$file['saved_as'];

					// Resize the image

	                $width=200;
        			Image::load($filepath) ->resize($width, null, true) ->save($filepath);

				}
			
				//User created date
				$user->dateadded = date('Y-m-d H:i:s');
				//Saves user to DB
				$user->save();

				//Response upon form submission
				Session::set_flash('success', 'User Added');
			
				//Redirect upon form submission
				Response::redirect('/');
			} else {			
				
				Session::set_flash('error', $val->show_errors());
				//Redirect upon form submission
				Response::redirect('/');
			
			}
		}
		//Pagination config
		$config = array(
			'pagination_url' => 'user/index/',
			'total_items'    => Model_User::count(),
			'per_page'       => 5,
			'uri_segment'    => 'page',
		);

		$pagination = Pagination::forge('userpagination', $config);

		$data['users'] = Model_User::query()
									->rows_offset($pagination->offset)
									->rows_limit($pagination->per_page)
									->get();

		// we pass the object, it will be rendered when echo'd in the view
		$data['pagination'] = $pagination;
        $this->template->title = 'Coding Challenge';
        $this->template->content = View::forge('user/index', $data, false);
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
}
