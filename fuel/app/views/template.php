<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, inistial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">
	
	<title><?php echo $title; ?></title>

	<?php echo Asset::css('bootstrap.css'); ?>
</head>
<body>
<div class="container">
<h1>Users</h1>
<?php echo $content; ?>
</div>
</body>
</html>
