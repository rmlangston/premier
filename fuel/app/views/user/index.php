<?php if(Session::get_flash('success')) : ?>
	<div class="alert alert-success">
		<?php echo Session::get_flash('success'); ?>
	</div>
<?php endif; ?>
<?php if(Session::get_flash('error')) : ?>
        <div class="alert alert-danger">
                <?php echo Session::get_flash('error'); ?>
        </div>
<?php endif; ?>
<h3>Add User</h3>
<?php echo Form::open(array('action' => '/user/index', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal')); ?>
	<div class="form-group">
		<?php echo Form::label('Name', 'name'); ?>
		<?php echo Form::input('name', Input::post('name', isset($user) ? $user->name : ''), array('class' => 'form-control')); ?>
	</div>
        <div class="form-group">
                <?php echo Form::label('Image', 'image'); ?>
                <?php echo Form::file('image'); ?>
        </div>
	<div class="form-group">
		<?php echo Form::submit('send', 'Submit', array('class' => 'btn btn-default')); ?>
	</div>
<?php echo Form::close(); ?>

<div class="row">
<table class="table table-hover">
<thead>
<tr>
<th scope="col">ID</th>
<th scope="col">Name</th>
<th scope="col">Date Added</th>
<th scope="col">Picture</th>
</tr>
</thead>
<tbody>
<?php foreach($users as $user) : ?>
<tr>
<td><?php echo $user->id; ?></td>
<td><?php echo $user->name; ?></td>
<td><?php echo $user->dateadded; ?> </td>
<?php if(!is_null($user->image)) : ?>
<td><?php echo Html::img('files/'.$user->image, array('class' => 'img-responsive')); ?></td>
<?php else : ?>
<td>No Image</td>
<?php endif; ?>
</tr>
<?php endforeach; ?>
</tbody>
</table>
<div class="pagination"><?php echo $pagination; ?></div>
</div>
